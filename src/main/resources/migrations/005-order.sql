create Table `order_table`(
    id           INT UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
    customer_id   INT UNSIGNED NOT NULL,
    order_date         DATE NOT NULL,
          order_status VARCHAR(32) NOT NULL
);