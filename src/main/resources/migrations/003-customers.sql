create table customers(
id int unsigned not null auto_increment primary key,
name varchar(32) not null,
surname varchar(32) not null,
email varchar(64) not null,
gender varchar(10)  not null,
date_of_birth DATE
);

