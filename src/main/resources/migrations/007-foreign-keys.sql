ALTER TABLE articles
ADD FOREIGN KEY (brand_id) REFERENCES brands(id);

ALTER TABLE `order_table`
ADD FOREIGN KEY (customer_id) REFERENCES customers(id);

ALTER TABLE order_stock
ADD FOREIGN KEY (order_id) REFERENCES `order_table`(id);

ALTER TABLE order_stock
ADD FOREIGN KEY (stock_id) REFERENCES stock(id);

ALTER TABLE stock
ADD FOREIGN KEY (article_id) REFERENCES articles(id);

