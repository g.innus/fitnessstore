CREATE TABLE articles
(
	id                  INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	number              INT UNSIGNED NOT NULL,
	description         VARCHAR(64) NOT NULL,
	brand_id             INT UNSIGNED
);


