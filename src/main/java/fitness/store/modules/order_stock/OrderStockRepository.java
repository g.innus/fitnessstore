package fitness.store.modules.order_stock;

import fitness.store.modules.order.Order;
import fitness.store.modules.stock.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public
interface OrderStockRepository extends JpaRepository<OrderStock, Long> {
    Optional<OrderStock> findByOrderId(Long id);
    Optional<OrderStock> findByStockId(Long id);

}
