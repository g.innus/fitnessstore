package fitness.store.modules.order_stock;


import fitness.store.modules.order.Order;
import fitness.store.modules.order.OrderRepository;
import fitness.store.modules.order.OrderStatus;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class OrderStockService {
    @Autowired
    private OrderStockRepository orderStockRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Transactional(readOnly = true)
    public List<OrderStock> findAll() {
        return orderStockRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public OrderStock save(final OrderStock orderStock) {
        Long stockId = orderStock.getStockId();
        Optional<OrderStock> existingOrder= orderStockRepository.findByStockId(stockId);
        if (existingOrder.isPresent())
            throw new RuntimeException("Order is not available");
        Long orderId = orderStock.getOrderId();
        if (deleteAccess(orderId))
            throw new RuntimeException("Order can not be changed anymore");

        System.out.println(orderStock);
        Validate.notNull(orderStock, "stock status is undefined");
        //    Validate.notBlank(orderStock.getOrderStatus(), "order status is blank for Order stock '%s'", orderStock);
        return orderStockRepository.save(orderStock);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(final long orderStockId) {
        if (deleteAccess(orderStockId))
            throw new RuntimeException("Order can not be changed anymore");

        orderStockRepository.deleteById(orderStockId);
    }

    public boolean deleteAccess(Long id) {
        Optional<Order> orderOptional = orderRepository.findById(id);
        if (!orderOptional.isPresent())
            throw new RuntimeException("Order not found");
        OrderStatus orderStatus = orderOptional.get().getOrderStatus();
        if (orderStatus == OrderStatus.SHIPPED || orderStatus == OrderStatus.APPROVED) {
            return false;
        } else {
            return true;
        }
    }

}

