package fitness.store.modules.articles;

import java.util.List;


import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Transactional(readOnly = true)
    public List<Article> findAll() {
        return articleRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Article save(final Article article) {

        // example validate usages
        Validate.notNull(article, "article is undefined");
        Validate.notBlank(article.getDescription(), "name is blank for article '%s'", article);

        return articleRepository.save(article);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(final long articleId) {
        articleRepository.deleteById(articleId);
    }
}
