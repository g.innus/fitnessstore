package fitness.store.modules.articles;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "articles")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private Integer number;

    @Column
    private String description;

    @Column
    private Integer brandId;

    public Article() {
    }

    public Article(final Integer number, final String description, final Integer brandId) {
        this.number = number;
        this.description = description;
        this.brandId = brandId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBrand_id(Integer brandId) {
        this.brandId = brandId;
    }

    public Long getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public String getDescription() {
        return description;
    }

    public Integer getBrandId() {
        return brandId;
    }

    @Override
    public String toString() {
        return "Article{" + "id=" + id + ", number='" + number+ '\'' + ", description='" + description+ '\'' + ", brandId=" + brandId + '}';
    }
}
