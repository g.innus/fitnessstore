package fitness.store.modules.brand;

import fitness.store.modules.brand.Brand;
import fitness.store.modules.brand.BrandRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BrandsService {
    @Autowired
    BrandRepository brandRepository;

    @Transactional (readOnly = true)
    public List<Brand> findAll(){
        return brandRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Brand save(final Brand brand){
        System.out.println(brand);

        Validate.notNull(brand, "brand is undefined");
        Validate.notBlank(brand.getName(), "name is blank for author '%s'", brand);

        return brandRepository.save(brand);
    }
    @Transactional(rollbackFor = Exception.class)
    public void delete(final long brandId) {
        brandRepository.deleteById(brandId);
    }

}
