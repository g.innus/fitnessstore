package fitness.store.modules.brand;

import fitness.store.modules.brand.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface BrandRepository extends JpaRepository<Brand, Long> {

}
