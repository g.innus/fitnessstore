package fitness.store.modules.order;

public enum OrderStatus {
    QUEUE,
    NOT_APPROVED,
    APPROVED,
    SHIPPED
}
