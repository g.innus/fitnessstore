package fitness.store.modules.order;

import fitness.store.modules.order_stock.OrderStock;
import fitness.store.modules.order_stock.OrderStockRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderStockRepository orderStockRepository;


    @Transactional(readOnly = true)
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Order save(final Order order) {
        System.out.println(order);

        Validate.notNull(order, "order is undefined");
        Validate.notNull(order.getDate(), "date is blank for order '%s'", order);

        return orderRepository.save(order);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(final Long orderID) {

        orderRepository.deleteById(orderID);
    }
    @Transactional(rollbackFor = Exception.class)
    public void update(final Long id, OrderStatus orderStatus) {
        Optional<Order> shipment = orderRepository.findById(id);
        if (shipment.isPresent()) {
            Order updateOrder = shipment.get();
            updateOrder.setOrderStatus(orderStatus);
            orderRepository.save(updateOrder);
        } else {
            throw new RuntimeException("order not found");
        }


    }


}
