package fitness.store.modules.customers;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CustomersService {

    @Autowired
    private CustomersRepository customersRepository;

    @Transactional(readOnly = true)
    public List<Customers> findAll() {
        return customersRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Customers save(final Customers customers) {
        Validate.notNull(customers, "Customer is not found");
        Validate.notBlank(customers.getName(), "Customer is dead '%s'", customers);
        Customers customerOriginal = customersRepository.findById(customers.getId()).orElse(null);
        Validate.notNull(customerOriginal, "customer whos id is not found %s", customers.getId());
        customers.setEmail(customerOriginal.getEmail());

        return customersRepository.save(customers);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(final long customerID) {

        customersRepository.deleteById(customerID);
    }

    @Transactional(rollbackFor = Exception.class)
    public Customers create(Customers customers) {
        Validate.notNull(customers, "Customer is not found");
        Validate.notBlank(customers.getName(), "Customer is dead", customers);
        return customersRepository.save(customers);
    }
}
