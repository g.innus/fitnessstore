package fitness.store.modules.customers;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface CustomersRepository extends JpaRepository<Customers, Long> {
}
