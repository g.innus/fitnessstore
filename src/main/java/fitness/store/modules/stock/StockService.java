package fitness.store.modules.stock;

import java.util.List;
import java.util.Optional;


import fitness.store.modules.order.Order;
import fitness.store.modules.order_stock.OrderStock;
import fitness.store.modules.order_stock.OrderStockRepository;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class StockService {

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private OrderStockRepository orderStockRepository;

    @Transactional(readOnly = true)
    public List<Stock> findAll() {
        return stockRepository.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Stock save(final Stock stock) {
        if (stock.getId() != null) {
            Optional<OrderStock> orderStock = orderStockRepository.findById(stock.getId());
            if (orderStock.isPresent())
                throw new RuntimeException("Order cant be updated, cause the order is already placed");
            Validate.notNull(stock, "stock is undefined");
            stockRepository.deleteById(stock.getId());
        }


        return stockRepository.save(stock);

    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(final Long stockId) {
        Optional<OrderStock> orderStock = orderStockRepository.findById(stockId);
        stockRepository.deleteById(stockId);
        if (orderStock.isPresent())
            throw new RuntimeException("Order cant be deleted, cause the order is already placed");
    }


}
