package fitness.store.controllers;


import fitness.store.modules.customers.Customers;
import fitness.store.modules.customers.CustomersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

@RestController
@RequestMapping("/customers")
public class CustomersController {

    @Autowired
    private CustomersService customersService;

    @GetMapping
    public List<Customers> findAll() {
        return customersService.findAll();
    }

    @PutMapping
    public Customers create(@RequestBody Customers customers) {
        return customersService.create(customers);
    }

    @PatchMapping
    public Customers update(@RequestBody Customers customers) {
        return customersService.save(customers);
    }

    @DeleteMapping("/{CustomerID}")
    public void delete(@PathVariable long CustomerID) {
        LOGGER.info(String.format("User has requested deletion of customers with ID '%s'", customersService));
        customersService.delete(CustomerID);
    }
}
