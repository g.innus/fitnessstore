package fitness.store.controllers;


import fitness.store.modules.articles.Article;
import fitness.store.modules.articles.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticlesController {
    @Autowired
    ArticleService articleService;

    @GetMapping
    public List<Article> getAll() {
        return articleService.findAll();
    }

    @PutMapping
    public Article create(@RequestBody Article article) {
        return articleService.save(article);
    }

    @PatchMapping
    public Article update(@RequestBody Article article) {
        return articleService.save(article);
    }

    @DeleteMapping("/articleId")
    public void delete(@PathVariable long articleId) {
         articleService.delete(articleId);
    }



}