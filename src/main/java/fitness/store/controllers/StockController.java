package fitness.store.controllers;


import fitness.store.modules.stock.Stock;
import fitness.store.modules.stock.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/stock")
public class StockController {
    @Autowired
    StockService stockService;

    @GetMapping
    public List<Stock> getAll() {
        return stockService.findAll();
    }

    @PutMapping
    public Stock create(@RequestBody Stock stock) {
        return stockService.save(stock);
    }

    @PatchMapping
    public Stock update(@RequestBody Stock stock) {
        return stockService.save(stock);
    }

    @DeleteMapping("/stockId")
    public void delete(@PathVariable Long stockId) {
        stockService.delete(stockId);
    }



}