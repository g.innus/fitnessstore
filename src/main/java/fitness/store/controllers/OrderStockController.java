package fitness.store.controllers;


import fitness.store.modules.order_stock.OrderStock;
import fitness.store.modules.order_stock.OrderStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

@RestController
@RequestMapping("/order_stock")
public class OrderStockController {
    @Autowired
    private OrderStockService orderStockService;

    @GetMapping
    public List<OrderStock> getAll() {
        return orderStockService.findAll();
    }

    @PutMapping
    public OrderStock create(@RequestBody OrderStock orderStock) {
        return orderStockService.save(orderStock);
    }

    @PatchMapping
    public OrderStock update(@RequestBody OrderStock orderStock) {
        return orderStockService.save(orderStock);
    }

    @DeleteMapping("/{order_stockId}")
    public void delete(@PathVariable long order_stockId) {
        LOGGER.info(String.format("User has requested deletion of customers with ID '%s'", orderStockService));
        orderStockService.delete(order_stockId);
    }
}
