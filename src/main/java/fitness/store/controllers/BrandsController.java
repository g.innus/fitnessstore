package fitness.store.controllers;

import fitness.store.modules.brand.Brand;
import fitness.store.modules.brand.BrandsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/brands")
public class BrandsController {
    @Autowired
    BrandsService brandsService;
    @GetMapping
    public List<Brand> getAll(){
        return brandsService.findAll();
    }
    @PutMapping
    public Brand create(@RequestBody Brand brand){
        return brandsService.save(brand);
    }
    @DeleteMapping("/{brandId}")
    public void delete(@PathVariable long brandId) {
        brandsService.delete(brandId);
    }
    @PatchMapping
    public Brand update(@RequestBody Brand brand) {
        return brandsService.save(brand);
    }
}
