package fitness.store.controllers;


import fitness.store.modules.order.Order;
import fitness.store.modules.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.hibernate.bytecode.BytecodeLogger.LOGGER;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping
    public List<Order> getAll() {
        return orderService.findAll();
    }

    @PutMapping
    public Order create(@RequestBody Order order) {
        return orderService.save(order);
    }

    @PatchMapping
    public Order update(@RequestBody Order order) {
        return orderService.save(order);
    }

    @DeleteMapping("/{orderID}")
    public void delete(@PathVariable long orderID) {
        LOGGER.info(String.format("User has requested deletion of customers with ID '%s'", orderService));
        orderService.delete(orderID);
    }

}
